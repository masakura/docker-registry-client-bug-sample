const drc = require('docker-registry-client');

// Error: invalid repository name, may only contain [a-z0-9_.-] characters: docker-registry-client-bug-sample/image
//     at Object.parseRepo (/home/masakura/repos/masakura/docker-registry-client-bug-sample/node_modules/docker-registry-client/lib/common.js:225:15)
//     at new RegistryClientV2 (/home/masakura/repos/masakura/docker-registry-client-bug-sample/node_modules/docker-registry-client/lib/registry-client-v2.js:998:28)
//     at Object.createClient [as createClientV2] (/home/masakura/repos/masakura/docker-registry-client-bug-sample/node_modules/docker-registry-client/lib/registry-client-v2.js:1593:12)
//     at Object.<anonymous> (/home/masakura/repos/masakura/docker-registry-client-bug-sample/bug.js:5:5)
//     at Module._compile (module.js:571:32)
//     at Object.Module._extensions..js (module.js:580:10)
//     at Module.load (module.js:488:32)
//     at tryModuleLoad (module.js:447:12)
//     at Function.Module._load (module.js:439:3)
//     at Module.runMain (module.js:605:10)
//
drc.createClientV2({
  name: 'registry.gitlab.com/masakura/docker-registry-client-bug-sample/image'
})
  .listTags((err, tags) => console.log(tags));
